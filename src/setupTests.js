//https://testing-library.com/docs/dom-testing-library/api-configuration

import '@testing-library/jest-dom/extend-expect';

// jest-dom adds custom jest matchers for asserting on DOM nodes.
// allows you to do things like:
// expect(element).toHaveTextContent(/react/i)
// learn more: https://github.com/testing-library/jest-dom
import '@testing-library/jest-dom';

// https://testing-library.com/docs/example-react-router
import React from "react";
import { Router, Route } from "react-router-dom";
import { render } from "@testing-library/react";
import { createMemoryHistory } from "history";


  //example to overrule the default attribute for test id
//import { configure } from '@testing-library/react'
//configure({ testIdAttribute: 'data-my-test-id' })

//https://medium.com/@aarling/mocking-a-react-router-match-object-in-your-component-tests-fa95904dcc55
export default function renderWithRouter(
  ui,
  {
    route = "/",
    history = createMemoryHistory({ initialEntries: [route] })
  } = {}
) {
  return {
    ...render(<Router history={history}>{ui}</Router>),
    history
  };
}

export function renderWithRouterMatch(
    ui,
    {
      path = "/",
      route = "/",
      history = createMemoryHistory({ initialEntries: [route] })
    } = {}
  ) {
    return {
      ...render(
        <Router history={history}>
          <Route path={path} component={ui} />
        </Router>
      )
    };
  }

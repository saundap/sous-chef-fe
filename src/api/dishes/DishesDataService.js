import axios from "axios"
import { API_URL } from '../../Constants.js'

export const retrieveAllDishes = async () => {
    console.log('calling retrieveAllDishes service')
    return axios.get(`${API_URL}/dishes`)
}

export const retrieveDish = async (id) => {
    console.log('calling retrieveDish service')
    return axios.get(`${API_URL}/dishes/${id}`)
}

export const createDish = async (username, dish) => {
    console.log('calling retrieveDish service')
    return axios.post(`${API_URL}/dishes`, dish)
}

export const retrieveDishContainingIngredient = async (username, ingredient) => {
    console.log('calling retrieveDishContainingIngredient service for', ingredient)
    return axios.get(`${API_URL}/dishes`, 
        {
            params: {
                ingredient: ingredient
            }
        }
    )
}
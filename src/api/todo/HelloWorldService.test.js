import axios from 'axios';
import { callHelloWorldService, URL } from './HelloWorldService.js'

jest.mock('axios');

describe('callHelloWorldServiceBean', () => {
    test('callHelloWorldServiceBean successfully data from an API', async () => {
        
        //Arrange
        const data = {message: 'hello world'};
        axios.get.mockImplementationOnce(() => Promise.resolve(data));
        
        //Act & Assert
        await expect(callHelloWorldService('testname')).resolves.toEqual(data);
        expect(axios.get).toHaveBeenCalledWith(URL+'/testname');
    });
   
    test('callHelloWorldService erroneously data from an API', async () => {
        
        //Arrange      
        const errorMessage = 'Network Error';
        axios.get.mockImplementationOnce(() =>
          Promise.reject(new Error(errorMessage)),
        );

        //Act & Assert
        await expect(callHelloWorldService()).rejects.toThrow(errorMessage);

    });
  });
import axios from "axios"
import { API_URL } from '../../Constants.js'

export const URL = `${API_URL}/helloworldbean`
//export const URL = `${API_URL}/helloworldbeanerror` //retreive mocke error back from mock error resource

//example of hardcoding headers into the get call, rather than using axios.interceptors (see AuthenticationService.js)
let username = 'andy'
let password = 'password'  
let basicAuthHeader = 'Basic '+ window.btoa(username + ":" + password)
const headers = {
                  headers : {
                    authorization: basicAuthHeader
                  }
                }

//async call with promise (axios has promises abstracted)
 export const callHelloWorldService = async (name) => {
    console.log('calling hello world service bean')
      //return await axios.get(`${URL}/${name}`, headers) //basicAuth
      return await axios.get(`${URL}/${name}`) //use the axios interceptors rely on Jwt token to be added here
}
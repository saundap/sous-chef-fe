import axios from 'axios';
import { retrieveAllTodos, retrieveTodo, updateTodo, createTodo, deleteTodo, JPA_API_URL } from './TodoDataService.js'
import { JPA_API_URL as API } from '../../Constants.js'

jest.mock('axios');

describe('Todo Service Tests', () => {
    test('call retrieveAllTodos successfully data from an API', async () => {
        
        //Arrange
        const data = {data: [{id: 0, username: "andy", description: "Learn to Dance", targetDate: 1607863270787, done: false}]};
        axios.get.mockImplementationOnce(() => Promise.resolve(data));
        
        //Act & Assert
        await expect(retrieveAllTodos('testname')).resolves.toEqual(data);
        expect(axios.get).toHaveBeenCalledWith(API+'/users/testname/todos');
    });
   
    test('call retrieveAllTodos erroneously data from an API', async () => {
        
        //Arrange      
        const errorMessage = 'Network Error';
        axios.get.mockImplementationOnce(() =>
          Promise.reject(new Error(errorMessage)),
        );

        //Act & Assert
        await expect(retrieveAllTodos('testname')).rejects.toThrow(errorMessage);
    });

    test('call delete Todo successfully data from an API', async () => {
        
      //Arrange
      const data = {data:{}};
      axios.delete.mockImplementationOnce(() => Promise.resolve(data));
      
      //Act & Assert
      await expect(deleteTodo('testname', 1)).resolves.toEqual(data);
      expect(axios.delete).toHaveBeenCalledWith(API+'/users/testname/todo/1');
    });
 
    test('call delete Todo erroneously data from an API', async () => {
      
      //Arrange      
      const errorMessage = 'Network Error';
      axios.delete.mockImplementationOnce(() =>
        Promise.reject(new Error(errorMessage)),
      );

      //Act & Assert
      await expect(deleteTodo('testname', 1)).rejects.toThrow(errorMessage);
    });    

    test('call retrieveTodo successfully data from an API', async () => {
        
      //Arrange
      const data = {data: [{id: 0, username: "andy", description: "Learn to Dance", targetDate: 1607863270787, done: false}]};
      axios.get.mockImplementationOnce(() => Promise.resolve(data));
      
      //Act & Assert
      await expect(retrieveTodo('testname', 0)).resolves.toEqual(data);
      expect(axios.get).toHaveBeenCalledWith(API+'/users/testname/todo/0');
    });
 
    test('call retrieveTodo erroneously data from an API', async () => {
      
      //Arrange      
      const errorMessage = 'Network Error';
      axios.get.mockImplementationOnce(() =>
        Promise.reject(new Error(errorMessage)),
      );

      //Act & Assert
      await expect(retrieveTodo('testname', 0)).rejects.toThrow(errorMessage);
    });

    test('call updateTodo successfully data from an API', async () => {
        
      //Arrange
      const data = { status: 200, data: {id: 0, username: "andy", description: "Learn to Dance", targetDate: 1607863270787, done: false}};
      axios.put.mockImplementationOnce(() => Promise.resolve(data));
      
      //Act & Assert
      await expect(updateTodo('testname', 0, data.data)).resolves.toEqual(data);
      expect(axios.put).toHaveBeenCalledWith(API+'/users/testname/todo/0', data.data);
    });
 
    test('call updateTodo erroneously data from an API', async () => {
      
      //Arrange      
      const errorMessage = 'Network Error';
      axios.put.mockImplementationOnce(() =>
        Promise.reject(new Error(errorMessage)),
      );

      //Act & Assert
      await expect(updateTodo('testname', 0, {})).rejects.toThrow(errorMessage);
    });    

    test('call createTodo successfully data from an API', async () => {
        
      //Arrange
      const data = { status: 201, data: {username: "andy", description: "Learn to Dance", targetDate: 1607863270787, done: false}};
      axios.post.mockImplementationOnce(() => Promise.resolve(data));
      
      //Act & Assert
      await expect(createTodo('testname', 0)).resolves.toEqual(data);
      expect(axios.post).toHaveBeenCalledWith(API+'/users/testname/todo', 0);
    });
 
    test('call createTodo erroneously data from an API', async () => {
      
      //Arrange      
      const errorMessage = 'Network Error';
      axios.post.mockImplementationOnce(() =>
        Promise.reject(new Error(errorMessage)),
      );

      //Act & Assert
      await expect(createTodo('testname', 0)).rejects.toThrow(errorMessage);
    }); 

  });
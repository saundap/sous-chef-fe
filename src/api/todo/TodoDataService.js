import axios from "axios"
import { API_URL, JPA_API_URL } from '../../Constants.js'

export const retrieveAllTodos = async (username) => {
    console.log('calling retrieveAllTodos service')
    return axios.get(`${JPA_API_URL}/users/${username}/todos`)
}

export const retrieveTodo = async (username, id) => {
    console.log('calling retrieveTodo service')
    return axios.get(`${JPA_API_URL}/users/${username}/todo/${id}`)
}

export const deleteTodo = async (username, id) => {
    console.log('calling deleteTodo service')
    return axios.delete(`${JPA_API_URL}/users/${username}/todo/${id}`)
}

export const updateTodo = async (name, id, todo) => {
    console.log('updatingTodo '+name, id, todo)
    return axios.put(`${JPA_API_URL}/users/${name}/todo/${id}`, todo);
}

export const createTodo = async (name, todo) => {
    console.log('creatingTodo '+name, todo)
    return axios.post(`${JPA_API_URL}/users/${name}/todo`, todo);
}
import React from 'react';  
import './css/popup.css';  
import AuthenticationService from '../todo/AuthenticationService.js'
import { retrieveDish } from '../../api/dishes/DishesDataService.js'

class Popup extends React.Component {  
    constructor(props) {
        super(props)
        console.log('popup constructor')
        this.state = {
            dishes: []
        }
    }
    
    async componentDidMount() {
        console.log('popup componentDidMount')
        this.props.id.map (
            id => retrieveDish(id)
            .then(response => {
                if(response.data.dish != null)
                this.setState (state => {
                    const dishes = [...state.dishes, response.data.dish];
                    return {dishes}              
                })
            })
        )
        await new Promise(resolve => { setTimeout(resolve, 1000); });
        console.log('popup componentDidMount complete')
    }


    render() {  
        return (  
            <div className='popup'>  
                <div className='popup_inner'>  
                    {this.state.dishes.map (
                        dish => 
                            <div>{dish}</div> 
                        )
                    } 
                    <button onClick={this.props.closePopup}>close me</button>  
                </div>  
            </div>  
        );  
    }  
}  

export default Popup;
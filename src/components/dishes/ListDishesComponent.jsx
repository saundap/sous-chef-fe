import React, {Component} from 'react'
import { retrieveAllDishes } from '../../api/dishes/DishesDataService.js'
import AuthenticationService from '../todo/AuthenticationService.js'
import Popup from './Popup.js'
import {withRouter} from "react-router-dom";
import './css/Dish.css'

class ListDishesComponent extends Component {
    constructor(props) {
        super(props)
        //console.log('constructor')
        this._isMounted = false;
        this.state = {
            dishes :  [],
            message: null,
            dishesSelected:[],
            showPopup: false
        }

        this.togglePopup = this.togglePopup.bind(this)
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSuccessResponse = this.handleSuccessResponse.bind(this)
        this.refreshDishes = this.refreshDishes.bind(this)
        this.addDishClicked = this.addDishClicked.bind(this)
    }

    togglePopup() {  
        console.log(this.state.dishesSelected)

        this.setState({  
             showPopup: !this.state.showPopup  
        });  
    }  

    componentWillUnmount(){
        //console.log('componentWillUnmount')
        this._isMounted = false;
    }

    shouldComponentUpdate(nextProps, nextState){
        //console.log('shouldComponentUpdate')
        //console.log(nextProps)
        //console.log(nextState)
        return true
    }

    componentDidMount() {
        console.log('mount')
        this._isMounted = true;
        this.refreshDishes()
    }

    handleSuccessResponse(response, id){
        console.log('handleSuccessResponse')
        //console.log(response)
        this.setState({ 
            message: `Delete of dish ${id} successful` 
        })
        this.refreshDishes()
    }

    addDishClicked(){
        console.log('addDishClicked')
        this.props.history.push(`/dishes/-1`)       
    }

    deleteDishClicked(id){
        console.log('deleteDishClicked ' +id)
        let username = AuthenticationService.getLoggedInUserName()
        console.log(id + " " + username)
        deleteDish(username, id)
        .then(response => this.handleSuccessResponse(response, id))
        //not implemented .catch(error => this.handleErrorResponse(error))
    }

    viewDish(id){
        console.log('viewDish ' +id)
        this.props.history.push(`/dishes/${id}`)
    }
    
    refreshDishes() {
        console.log('refreshDishes')
        let username = AuthenticationService.getLoggedInUserName()
        retrieveAllDishes()
            .then(
                response => {
                    console.log(response)
                    this.setState ({
                        dishes: response.data
                    })
                }
            )
    }

    handleInputChange(event) {
        const target = event.target;
        var value = target.value;
        
        if(target.checked){
            this.state.dishesSelected[value] = value;   
        }else{
            this.state.dishesSelected.splice(value, 1);
        }
        
    }

    render() {
        //console.log('render')
            return (
                <div className="container">
                    <div className="ListDishesComponent">
                        <h1 data-testid="listDishesheader">List Dishes</h1>
                        <div className="form-row">
                            <div class="col-md-12 text-center">
                                <button className="btn btn-primary" data-testid="addDish" onClick={this.addDishClicked}>Add New Dish</button>
                                <button type="submit" class="btn btn-primary" onClick={()=>this.togglePopup()}>Submit</button>
                            </div>
                            {this.state.showPopup ?  
                                <Popup  
                                        id ={this.state.dishesSelected}
                                        closePopup={this.togglePopup}  
                                />  
                                : null  
                                }  
                        </div>                   
                        <div>
                            {this.state.message}
                        </div>
                        {this.state.message && <div className="alert alert-success" data-testid="deletedmessage">{this.state.message}</div>}
                        <table className="table" data-testid="dishestable">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Select</th>
                                    <th>Description</th>
                                    <th>Type</th>
                                    <th>Cook Time (minutes)</th>
                                </tr>
                            </thead>
                            <tbody>
                            {this.state.dishes.map (
                                dish => 
                                    <tr key={dish.id}>
                                        <td>{dish.id}</td>
                                        <input type="checkbox" value={dish.id} onChange={this.handleInputChange} />
                                        <td>
                                            <button data-testid={`dish-${dish.id}`} className="btn btn-light" onClick={() => this.viewDish(dish.id)}>{dish.dish}</button>
                                        </td>
                                        <td>{dish.type}</td>
                                        <td>{dish.cookTime.time}</td>
                                    </tr> 
                                )
                            }
                            </tbody>
                        </table>
                    </div>
                </div>
            )
    }

}

export default withRouter(ListDishesComponent);
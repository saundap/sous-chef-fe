import React, {Component} from 'react'
import { retrieveDish, retrieveDishContainingIngredient } from '../../api/dishes/DishesDataService.js'
import AuthenticationService from '../todo/AuthenticationService.js'
import './css/Dish.css'
import './Tooltip.js'
import Tooltip from './Tooltip.js'

class DishComponent extends Component {
    constructor(props) {
        super(props)
        console.log('constructor')
        this.state = {
            id: this.props.match.params.id,
            dish: "-",
            type: "-",
            cookTime: {},
            equipment: "",
            baseIngredients: [{}],
            ingredients: [{}]
        }

    }

    componentWillUnmount(){
        console.log('componentWillUnmount')
    }

    shouldComponentUpdate(nextProps, nextState){
        console.log('shouldComponentUpdate')
        //console.log(nextProps)
        //console.log(nextState)
        return true
    }

    componentDidMount() {
        console.log('componentDidMount')
        if(this.state.id == -1){
            return
        }

        let username = AuthenticationService.getLoggedInUserName()
        retrieveDish(this.state.id)
        .then(response => {
            console.log(response)
                this.setState({
                    dish: response.data.dish,
                    type: response.data.type,
                    cookTime: response.data.cookTime,
                    equipment: response.data.equipment,
                    baseIngredients: response.data.baseIngredients,
                    ingredients: response.data.ingredients
                })
        })
    }

    render() {
        console.log('parent')
        let props = {
            dish: this.state.dish,
            type: this.state.type,
            equipment: this.state.equipment,
            cookTime: this.state.cookTime,
            baseIngredients: this.state.baseIngredients,
            ingredients: this.state.ingredients
        }

        return (
            <div>
                <h1 data-testid="dishheader">Dish</h1>
                <div className="container">
                    <DishDetailsComponent {...props}/>
                </div>
            </div>
        )
    }

}

class DishDetailsComponent extends Component{
    constructor(props) {
        super(props)
        console.log('constructor')
    }

    render() {
        console.log(this.props  )
        return (
            <div>
                <header>{this.props.dish}</header>
                <div>{this.props.type}</div>
                <div>{this.props.equipment}</div>
                <div>
                    {this.props.cookTime.time}
                    {this.props.cookTime.type}
                </div>
                <div>
                    {this.props.baseIngredients.map((item, index) => (
                        <li key={index}>{item.name}</li>
                    ))}
                </div>
                <div style={{
                        textAlign:"left"
                      }}>
                <table>
                    <th>Name</th>
                    <th>Quantity</th>
                    <th>Metric</th>
                    {this.props.ingredients.map( (ingredient, index ) =>
                        RowComponent(ingredient, index)
                    )}
                </table>
    
                </div>
            </div>
        )
    }
}

function RowComponent(ingredient, index) {
    
    async function dishesWithIngredient () {
        console.log('dishesWithIngredient');
        let result = [];
        let username = AuthenticationService.getLoggedInUserName();

        return await retrieveDishContainingIngredient(username, ingredient.name)
        .then(response => {
            console.log('response from service');
            let item;
            for (item of response.data) {
                result.push({ 
                    id: item.id,
                    name: item.dish
                 });
            }
            let dish;
            for (dish of result) {
              console.log('dish found : '+dish.id +' '+dish.name);
            }
            return result;            
        })

    }

    return (
            <tr key={index}>
                <Tooltip content={dishesWithIngredient} direction='right'>
                    <td data-title="ingredient">{ingredient.name}</td>
                </Tooltip>
                <td data-title="quantity">{ingredient.quantity}</td>
                <td data-title="measurement">{ingredient.measurement}</td>  
            </tr>
    );
  }


export default DishComponent
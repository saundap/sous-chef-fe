import React, {Component} from 'react'
import { retrieveDish, createDish } from '../../api/dishes/DishesDataService.js'
import AuthenticationService from '../todo/AuthenticationService.js'
import { Formik, Field, Form, FieldArray } from "formik";
import './css/Dish.css'

class DishAddComponent extends Component {
    constructor(props) {
        super(props)
        console.log('constructor')
        this._isMounted = false;

        this.state = {
            id: -1,
            dish: "-",
            type: "-",
            cookTime: {},
            equipment: "",
            baseIngredients: [{}],
            ingredients: [{}]
        }

        this.handleSuccessResponse = this.handleSuccessResponse.bind(this)
        this.refreshDish = this.refreshDish.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
    }

    componentWillUnmount(){
        console.log('componentWillUnmount')
        this._isMounted = false;
    }

    shouldComponentUpdate(nextProps, nextState){
        //console.log('shouldComponentUpdate')
        //console.log(nextProps)
        //console.log(nextState)
        return true
    }

    componentDidMount() {
        console.log('componentDidMount')
        this._isMounted = true;
        console.log(this.state.id)
        if(this.state.id == -1){
            return
        }
    }

    onSubmit(values) {
        console.log('onSubmit')
        let username = AuthenticationService.getLoggedInUserName()
        
        let dish = {
            id: this.state.id,
            dish: values.dish,
            type: values.type,
            cookTime: values.cookTime,
            equipment: values.equipment,
            baseIngredients: values.baseIngredients,
            ingredients: values.ingredients
        }

        if (this.state.id == -1){
            console.log('CREATING A DISH')
            createDish(username, dish)
                .then(() => this.props.history.push('/dishes')) //execute post and then return to todos
        }
        
        console.log(values);
    }

    refreshDish(id) {
        console.log('refreshDish')
        let username = AuthenticationService.getLoggedInUserName()
        retrieveDish(id)
            .then(
                response => {
                    console.log(response)
                    this.setState ({
                        dish: response.data.dish,
                        type: response.data.type,
                        cookTime: response.data.cookTime,
                        equipment: response.data.equipment,
                        baseIngredients: response.data.baseIngredients,
                        ingredients: response.data.ingredients
                    })
                }
            )
    }

    handleSuccessResponse(response, id){
        console.log('handleSuccessResponse')
        //console.log(response)
        this.setState({ 
            message: `Delete of dish ${id} successful` 
        })
        this.refreshDish(id)
    }  

    render() {
        console.log('render main')

        let {dish,type,equipment,cookTime,baseIngredients,ingredients} = this.state
        
        return (
            <div>
                <h1 data-testid="dishheader">Dish</h1>
                <div className="container">
                <Formik initialValues = {{dish,type,equipment,cookTime,baseIngredients,ingredients}}
                                    onSubmit={this.onSubmit}
                                    enableReinitialize={true}
                    >
                        {
                            ({ values }) => (
                                <Form>
                                    <fieldset className="form-group"> {/** fieldset is css bootstrap element */}
                                        <label>Dish</label>
                                        <Field data-testid="dishInput" className="form-control" type="text" name="dish"></Field>
                                        <label>Type</label>
                                        <Field data-testid="typeInput" className="form-control" type="text" name="type"></Field>
                                        <label>Special Equipment</label>
                                        <Field data-testid="equipmentInput" className="form-control" type="text" name="equipment"></Field>
                                        <label>CookTime (minutes)</label>
                                        <Field data-testid="cookTimeInput" className="form-control" type="text" name="cookTime.time"></Field>
                                    </fieldset>
                                    <fieldset className="form-group"> {/** fieldset is css bootstrap element */}
                                        <label>Base ingredients</label>
                                        <FieldArray
                                            name="baseIngredients"
                                        >
                                            {({ remove, push }) => (
                                            <div>
                                                {values.baseIngredients.length > 0 &&
                                                    values.baseIngredients.map((friend, index) => (
                                                    <div className="row" key={index}>
                                                    <div className="col">
                                                        <label htmlFor={`baseIngredients.${index}.name`}>Name</label>
                                                        <Field
                                                        name={`baseIngredients.${index}.name`}
                                                        placeholder="Jane Doe"
                                                        type="text"
                                                        />
                                                    </div>
                                                    <div className="col">
                                                        <button
                                                        type="button"
                                                        className="secondary"
                                                        onClick={() => remove(index)}
                                                        >
                                                        X
                                                        </button>
                                                    </div>
                                                    </div>
                                                ))}
                                                <button
                                                type="button"
                                                className="secondary"
                                                onClick={() => push({ name: "" })}
                                                >
                                                Add Base Ingredient
                                                </button>
                                            </div>
                                            )}
                                        </FieldArray>
                                    </fieldset>
                                    <fieldset className="form-group">
                                    <label>Ingredients</label>
                                        <FieldArray
                                            name="ingredients"
                                        >
                                            {({ remove, push }) => (
                                            <div>
                                                {values.ingredients.length > 0 &&
                                                    values.ingredients.map((ingredient, index) => (
                                                    <div className="row" key={index}>
                                                    <div className="col">
                                                        <label htmlFor={`ingredients.${index}.name`}>Name</label>
                                                        <Field
                                                        name={`ingredients.${index}.name`}
                                                        placeholder="ingredient name"
                                                        type="text"
                                                        />
                                                    </div>
                                                    <div className="col">
                                                        <label htmlFor={`ingredients.${index}.quantity`}>Quantity</label>
                                                        <Field
                                                        name={`ingredients.${index}.quantity`}
                                                        placeholder="ingredient quantity"
                                                        type="text"
                                                        />
                                                    </div>
                                                    <div className="col">
                                                        <label htmlFor={`ingredients.${index}.measurement`}>Measurement</label>
                                                        <Field
                                                        name={`ingredients.${index}.measurement`}
                                                        placeholder="ingredient measurment"
                                                        type="text"
                                                        />
                                                    </div>
                                                    <div className="col">
                                                        <button
                                                        type="button"
                                                        className="secondary"
                                                        onClick={() => remove(index)}
                                                        >
                                                        X
                                                        </button>
                                                    </div>
                                                    </div>
                                                ))}
                                                <button
                                                type="button"
                                                className="secondary"
                                                onClick={() => push({ name: "" })}
                                                >
                                                Add Ingredient
                                                </button>
                                            </div>
                                            )}
                                        </FieldArray>
                                    </fieldset>
                                    <br />
                                    <button data-testid="submit" type="submit" name="submit" className="btn btn-primary">Submit</button>                                        
                                </Form>
                            )
                        }
                    </Formik>
                </div>
            </div>
        )
    }

}

export default DishAddComponent
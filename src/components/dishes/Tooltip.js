import React, { useState } from "react";
import "./css/Tooltip.css";

let texts;

const Tooltip = (props) => {
  let timeout;
  const [active, setActive] = useState(false);
  
  const showTip = async () => {   
    timeout = setTimeout(() => {
      setActive(true);
    }, props.delay || 400);

    return await props.content()
    .then(function(item) {
      texts = item;
    });
  };

  const hideTip = () => {
    clearInterval(timeout);
    setActive(false);
  };

  return (
    <div
      className="Tooltip-Wrapper"
      // When to show the tooltip
      onMouseEnter={showTip}
      onMouseLeave={hideTip}
    >
      {/* Wrapping */}
      {props.children}
      {active && (
        <div className={`Tooltip-Tip ${props.direction || "top"}`}>
          {/* Content */}
          <table className="table" data-testid="todotable">
            <thead>
              <tr>
                  {/**<th>Id</th>*/}
                  <th>Name</th>
              </tr>
            </thead>
            <tbody>
            {texts.map (
              text => 
                    <tr key={text.id}>
                        {/**<td>{text.id}</td>*/}
                        <td>{text.name}</td>
                    </tr> 
                )
            } 
            </tbody>
          </table>
        </div>
      )}
    </div>
  );
};

export default Tooltip;

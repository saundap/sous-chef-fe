import React from 'react';
import '@testing-library/jest-dom'
import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import Counter from './Counter'

console.log("file paths are "+process.cwd())
console.log("current working directory is "+__dirname)

describe('Counter tests', () => {

    it('Counter page renders as expected at load', () => {   
      const tree = render(<Counter />);  
      expect(tree).toMatchSnapshot();
     })
  
    it('Counter page renders increment by 1', async () => {
      
      const { getByTestId } = render(<Counter />);
      const btnIncrementBy1 = getByTestId(`button-increment-1`)
      await waitFor(() => btnIncrementBy1);
      fireEvent.click(btnIncrementBy1);
      const result = getByTestId(`count-total`)
      expect(result).toHaveTextContent(1)
    })

    it('Counter page renders decrement by 1', async () => {
      
      const { getByTestId } = render(<Counter />);
      const btnIncrementBy1 = getByTestId(`button-decrement-1`)
      await waitFor(() => btnIncrementBy1);
      fireEvent.click(btnIncrementBy1);
      const result = getByTestId(`count-total`)
      expect(result).toHaveTextContent(-1)
    })

    it('Counter page resets to 0', async () => {
      
      const { getByTestId } = render(<Counter />);
      const btnIncrementBy1 = getByTestId(`button-decrement-1`)
      await waitFor(() => btnIncrementBy1);
      fireEvent.click(btnIncrementBy1);
      expect(getByTestId(`count-total`)).toHaveTextContent(-1)
      fireEvent.click(getByTestId(`reset`));
      expect(getByTestId(`count-total`)).toHaveTextContent(0)
    })

})


import React, {Component} from 'react'
import PropTypes from 'prop-types'
import './Counter.css'


//in the counter component we have a counterbutton component embedded
//a prop for counterbutton is incrementFunction and decrementFunction
//these are props that pass the Counter functions incrementParent and decrementParent to the counterbutton component
//so that when the counterbutton has an onclick change it's actually running a function
//defined in the parent Counter component, and thus changing the parent state
//this way we can have multiple CounterButton components of differing values (1, 5, 10 for example)
//and when each one is clikced they will alter the parent state keeping track of the overall counter value

class Counter extends Component {
    //Define the initial state in a constructor
    //state => counter == 0
    constructor() {
        super();

        this.state = {
            counter : 0
        }
        //react es6 currently prefers bind nested functions to the class
        this.incrementParent = this.incrementParent.bind(this)
        this.decrementParent = this.decrementParent.bind(this)
        this.reset = this.reset.bind(this)
        //formerly, binding is not required if we use the lambda function syntax for all nested functions in a class
        // render() -> becomes -> render = () => {}
        // increment() -> becomes -> increment = () => {}
    }

    render() {
        return (
            <div className="counter">
                My Todo Application
                <CounterButton by={1} incrementFunction={this.incrementParent} decrementFunction={this.decrementParent}/>
                <CounterButton by={5} incrementFunction={this.incrementParent} decrementFunction={this.decrementParent}/>
                <CounterButton by={10} incrementFunction={this.incrementParent} decrementFunction={this.decrementParent}/>
                <span className="counter-count" data-testid="count-total">{this.state.counter}</span>
                <div><button className="counter-reset" data-testid="reset" onClick={this.reset}>Reset</button></div>
            </div>
        )
    }

    reset() {
        console.log(`reset`);
        this.setState(
            {counter : 0}
        )
    }

    incrementParent(by) { //increment the state => counter + by
        console.log(`incrementParent from child - ${by}`);
        this.setState(
            (prevState) => {
                return {counter: prevState.counter + by}
            }
        )
    }

    decrementParent(by) { //decrement the state => counter - by
        console.log(`decrementParent from child - ${by}`);
        this.setState(
            (prevState) => {
                return {counter: prevState.counter - by}
            }
        )
    }       
}

class CounterButton extends Component {
    render() {
        return (
            <div className="counter">
                <button className="counter-button" data-testid={"button-increment-"+this.props.by} onClick={() => this.props.incrementFunction(this.props.by)}>+{this.props.by}</button>
                <button className="counter-button" data-testid={"button-decrement-"+this.props.by} onClick={() => this.props.decrementFunction(this.props.by)}>-{this.props.by}</button>
            </div>
        )
    }   
}
 
//Default property value if none is passed
CounterButton.defaultProps = {
    by : 1
}

//Type check on the passed properties, if not of type defined here then an error thrown to console logs
CounterButton.propTypes = {
    by : PropTypes.number
}

export default Counter
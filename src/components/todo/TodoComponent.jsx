import React, { Component } from 'react'
import moment from "moment";
import { Field, Form, Formik, ErrorMessage } from 'formik';
import '../../components/todo/css/Todo.css'
import { retrieveTodo, updateTodo, createTodo } from '../../api/todo/TodoDataService.js'
import AuthenticationService from './AuthenticationService';


class TodoComponent extends Component {
   
    constructor(props){
        super(props)

        this.state ={
            id: this.props.match.params.id,
            description: '',
            targetDate: moment(new Date()).format('YYYY-MM-DD')
        }
        
        this.onSubmit = this.onSubmit.bind(this)
        this.validate = this.validate.bind(this)

    }

    componentDidMount(){

        //https://www.w3schools.com/js/js_comparisons.asp
        // equal value and equal type -> USING A === (triple equals) will equate on data type - 
        // equal to -> using a == (double equals) is more flexible in JS and can handle different data types 

        //if (parseInt(this.state.id, 10) === -1 ){ //don't bother mounting and calling retrieve if id is -1 (-1 means for new record see backend save method)
        if (this.state.id == -1 ){ //don't bother mounting and calling retrieve if id is -1 (-1 means for new record see backend save method)         
            return
        }

        //populate todo component with the content of it's associated todo id for user
        let username = AuthenticationService.getLoggedInUserName()
        retrieveTodo(username, this.state.id)
        .then(response => {
                this.setState({
                    description: response.data.description,
                    targetDate: moment(response.data.targetDate).format('YYYY-MM-DD')
                })
        })
    }
    
    onSubmit(values) {
        console.log('onSubmit')

        //the Formik form and library takes the values from the input fields automatically and understands this
        //this function is a prop for the formik element below so it has access to the input fields values
        //this is a much simplier approach and useful tool over how we achieved similar for LogInComponent
        //LogInComponent required using the react onChange and a custom handler to interpret and capture the values
        //here with Formik the values argument passed is always the CURRENT content of the input fields within the Formik element
        let username = AuthenticationService.getLoggedInUserName()
        
        let todo = {
            id: this.state.id,
            description: values.description,
            targetDate: values.targetDate,
            done: false
        }

        //check if for post or put however the backend save method will create 
        //a new record even if a put is sent b/c the id would be
        //equal to -1 
        //https://www.w3schools.com/js/js_comparisons.asp
        // equal value and equal type -> USING A === (triple equals) will equate on data type - 
        // equal to -> using a == (double equals) is more flexible in JS and can handle different data types 
        //if (parseInt(this.state.id, 10) === -1) {
        if (this.state.id == -1){
            console.log('CREATING A TODO')
            createTodo(username, todo)
                .then(() => this.props.history.push('/todos')) //execute post and then return to todos
        }else{
            updateTodo(username, this.state.id, todo)
                .then(() => this.props.history.push('/todos')) //execute put and then return to todos
        }
        
        console.log(values);
    }

    validate(values) {
        //validate and return errors
        // test let errors = {description: 'description should have 4 characters'}
        let errors = {}
        console.log(values)

        if(!values.description){
            errors.description = 'Enter a Description'
        }else if(values.description.length<5) {
            errors.description = 'Enter at least 5 characters for Description'
        }
        
        if(!moment(values.targetDate).isValid()){
            errors.targetDate = 'Enter a valid target date'
        }
        return errors
    }

    render() {
        {/**Formik allows for initial values, can use state values 
            
            JS feature !: 
            let {description,targetDate} = this.state
            - Using destructing here to limit verbosity - list property names and then identify the object that
            contains those SAME names and the variables can resolve themselves.
            
            - initialValues below in the Formik element also use a similar technique to limit verbosity;
            populated by componentDidMount
        */}           
        let {description,targetDate} = this.state

        return(
            <div>
                <h1 data-testid="todoheader">Todo</h1>
                    <div className="container">
                        <Formik initialValues = {{description,targetDate}}
                                onSubmit={this.onSubmit}
                                validateOnChange={false}
                                validateOnBlur={false}
                                validate={this.validate}
                                enableReinitialize={true}
                        >
                            {
                                (props) => (
                                    <Form>
                                        <ErrorMessage   data-testid="descriptionError" 
                                                        name="description" 
                                                        component="div" 
                                                        className="alert alert-warning"/>
                                        <ErrorMessage   data-testid="targetDateError" 
                                                        name="targetDate" 
                                                        component="div" 
                                                        className="alert alert-warning"/>
                                        <fieldset className="form-group">  {/** fieldset is css bootstrap element */}
                                            <label>Description</label>
                                            <Field data-testid="descriptionInput" className="form-control" type="text" name="description"></Field>
                                            <small id="descriptionHelp" className="form-text text-muted">Must be greater than 4 characters.</small>
                                        </fieldset>
                                        <fieldset className="form-group"> {/** fieldset is css bootstrap element */}
                                            <label>Target Date</label>
                                            <Field data-testid="targetDateInput" className="form-control" type="date" name="targetDate"></Field>
                                        </fieldset>
                                        <button data-testid="submit" type="submit" name="submit" className="btn btn-primary">Submit</button>                                        
                                    </Form>
                                )
                            }
                        </Formik>
                    </div>
            </div>
            
        )

    }
}


export default TodoComponent
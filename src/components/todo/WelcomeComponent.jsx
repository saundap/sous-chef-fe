import React, {Component} from 'react'
import {Link} from 'react-router-dom'
import '../todo/css/Todo.css'
import { callHelloWorldService } from '../../api/todo/HelloWorldService.js'

class WelcomeComponent extends Component {
    constructor(props) {
        //console.log('constructor')

        super(props)
        this.retrieveWelcomeMessage = this.retrieveWelcomeMessage.bind(this)
        this.handleSuccessResponse = this.handleSuccessResponse.bind(this)
        this.handleErrorResponse = this.handleErrorResponse.bind(this)

        this.state = {
            welcomeMessage: null,
            errorMessage: null
        }    
    }

    retrieveWelcomeMessage(){
        //console.log('retrieving welcome message with hello world service')
        callHelloWorldService(this.props.match.params.name)
        .then(response => this.handleSuccessResponse(response))
        .catch(error => this.handleErrorResponse(error))
    }

    handleSuccessResponse(response){
        //console.log('handleSuccessResponse')
        //console.log(response)
        this.setState({ welcomeMessage: response.data.message })
    }

    handleErrorResponse(error){
        //console.log('handleErrorResponse')
        //console.log(error)
        let errorMessage = '';

        //for when an error has no response at all - such as an authorisation error
        if(error.message)
            errorMessage += error.message;

        //for when an error has a response at all - such as a business layer response error
        if (error.response && error.response.data) {
            //console.log(error.response);
            //console.log('error!: '+error.response.data.message);
            errorMessage += error.response.data.message
        }
        this.setState({ errorMessage: errorMessage })
    } 
    
    render() {
        //console.log('render')
        return (
                <>
                    <h1>Welcome!</h1>
                    <div className="container" data-testid="welcome">
                        Welcome {this.props.match.params.name}. You can manage your todos <Link to="/todos">here</Link>.
                    </div>
                    <div className="container">
                        Click here to get a customised welcome message.
                        <button data-testid="helloworldbutton" className="btn btn-success" onClick={this.retrieveWelcomeMessage}>Get Welcome Message</button>
                    </div>
                    {this.state.errorMessage ? (<ErrorMessage message={this.state.errorMessage}/>) : (<WelcomeMessage message={this.state.welcomeMessage}/>)}
                </>
                )
    }
}

class WelcomeMessage extends Component {   
    render() {
        //console.log('WelcomeMessage render')
        return (
            <div className="container" data-testid="welcomemessage">
                {this.props.message}
            </div>
        )
    }
}  

class ErrorMessage extends Component {
    render() {
        //console.log('ErrorMessage render')
        return (
            <div className="alert alert-warning" data-testid="errormessage">
                {this.props.message}
            </div>
        )
    }
}

export default WelcomeComponent
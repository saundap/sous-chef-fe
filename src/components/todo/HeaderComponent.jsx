import React, {Component} from 'react'
import {Link} from 'react-router-dom'
import { withRouter } from 'react-router';
import AuthenticationService from './AuthenticationService.js'

var loginPage = "/login"
const userName = AuthenticationService.getLoggedInUserName();
var welcomePage = `/welcome/${userName}`
var todoPage = "/todos"
var dishesPage = "/dishes"
var logoutPage = "/logout"

class HeaderComponent extends Component {
    //https://www.w3schools.com/bootstrap4/bootstrap_ref_all_classes.asp
    render() {
        const isUserLoggedIn = AuthenticationService.isUserLoggedIn();
        console.log(`isUserLoggedIn ${isUserLoggedIn}`)
                
        return (
                <header>
                    <nav className="navbar navbar-expand-md navbar-dark bg-dark">
                        <div><a className="navbar-brand" href="https://www.in28minutes.com" data-testid="branding">ToDo</a></div>
                            <ul className="navbar-nav">
                                {isUserLoggedIn && <li><Link className="nav-link" to={welcomePage} data-testid="home_link">Home</Link></li>}
                                {isUserLoggedIn && <li><Link className="nav-link" to={todoPage} data-testid="todo_link">Todo</Link></li>}
                                {isUserLoggedIn && <li><Link className="nav-link" to={dishesPage} data-testid="dishes_link">Dishes</Link></li>}
                            </ul>
                            <ul className="navbar-nav navbar-collapse justify-content-end">
                                {!isUserLoggedIn && <li><Link className="nav-link" to={loginPage} data-testid="login_link">Login</Link></li>}
                                {isUserLoggedIn && <li><Link className="nav-link" to={logoutPage} data-testid="logout_link" onClick={AuthenticationService.logout}>Logout</Link></li>}
                            </ul>
                    </nav>
                </header>
        )
    }
}

export default withRouter(HeaderComponent);
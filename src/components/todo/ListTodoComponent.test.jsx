import React from 'react'
import '@testing-library/jest-dom/extend-expect'
import { screen, waitFor } from '@testing-library/react'
import ListTodoComponent from './ListTodoComponent.jsx';
import { renderWithRouterMatch } from '../../setupTests.js'
import userEvent from '@testing-library/user-event'
import mockedAxios from 'axios'

jest.mock('axios');

describe('ListTodoComponent', function(){
    test('ListTodoComponent renders', async () => {
        //Arrange
        const dataGet = {data: [{id: 0, username: "andy", description: "Learn to Dance", targetDate: 1607863270787, done: false}]};
        mockedAxios.get.mockImplementationOnce(() => Promise.resolve(dataGet));

        sessionStorage.setItem('authenticatedUser', 'andy');
        const tree = renderWithRouterMatch(ListTodoComponent);

        //Act & Assert
        await screen.findByText(/List Todo/i);
        expect(tree).toMatchSnapshot();

        //screen.debug()
        //Redundant Checks
        expect(screen.getByTestId("listtodoheader")).toHaveTextContent('List Todo')
        expect(screen.getByTestId('todotable')).toBeInTheDocument()
        expect(screen.getByTestId("todotable")).toHaveTextContent('Learn to Dance')
    })

    test('Click Delete todos', async () => {
        //Arrange
        const dataGet = {data: [
            {id: 0, username: "andy", description: "Learn to Dance", targetDate: 1607863270787, done: false},
            {id: 1, username: "andy", description: "Learn to Code", targetDate: 1607863270787, done: false},
            {id: 2, username: "andy", description: "Learn to Cook", targetDate: 1607863270787, done: false}
        ]};
        mockedAxios.get.mockImplementation(() => Promise.resolve(dataGet));

        const dataDelete = {};
        mockedAxios.delete.mockImplementationOnce(() => Promise.resolve(dataDelete));        
        
        sessionStorage.setItem('authenticatedUser', 'andy');
        renderWithRouterMatch(ListTodoComponent);

        //Act
        await screen.findByText(/List Todo/i);
        await screen.findByText(/Learn to Dance/i);
        userEvent.click(screen.getByTestId('delete-0'))

        //Assert
        await screen.findByTestId('deletedmessage');
        expect(screen.getByTestId('deletedmessage')).toBeInTheDocument();
        expect(screen.getByTestId('deletedmessage')).toHaveTextContent(/Delete of todo 0 successful/i)
    })

    test('Click Update todos', async () => {
        //Arrange
        const dataGet = {data: [
            {id: 0, username: "andy", description: "Learn to Dance", targetDate: 1607863270787, done: false},
            {id: 1, username: "andy", description: "Learn to Code", targetDate: 1607863270787, done: false},
            {id: 2, username: "andy", description: "Learn to Cook", targetDate: 1607863270787, done: false}
        ]};
        mockedAxios.get.mockImplementationOnce(() => Promise.resolve(dataGet));

        sessionStorage.setItem('authenticatedUser', 'andy');
        renderWithRouterMatch(ListTodoComponent);
        
        //Act
        await screen.findByText(/List Todo/i);
        await screen.findByText(/Learn to Dance/i);
        userEvent.click(screen.getByTestId('update-0'))

    })

    test('Click Add todos', async () => {
        //Arrange
        const dataGet = {data: [
            {id: 0, username: "andy", description: "Learn to Dance", targetDate: 1607863270787, done: false},
            {id: 1, username: "andy", description: "Learn to Code", targetDate: 1607863270787, done: false},
            {id: 2, username: "andy", description: "Learn to Cook", targetDate: 1607863270787, done: false}
        ]};
        mockedAxios.get.mockImplementationOnce(() => Promise.resolve(dataGet));

        sessionStorage.setItem('authenticatedUser', 'andy');
        renderWithRouterMatch(ListTodoComponent);
        
        //Act
        await screen.findByTestId('addtodo');
        userEvent.click(screen.getByTestId('addtodo'))        

    })    
})
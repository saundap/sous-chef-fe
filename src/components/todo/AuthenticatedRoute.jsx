import React, {Component} from 'react'
import {Route, Redirect} from 'react-router-dom'
import AuthenticationService from './AuthenticationService'

var loginPage = "/login"


class AuthenticatedRoute extends Component {
    render() {
            if(AuthenticationService.isUserLoggedIn()){
               //Using a spread operator
               return <Route {...this.props}/>
            }else{
               return <Redirect to={loginPage}/>
            }
        }
}

export default AuthenticatedRoute
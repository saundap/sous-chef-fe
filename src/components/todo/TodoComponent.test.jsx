import React from 'react'
import '@testing-library/jest-dom/extend-expect'
import { screen, act, waitFor } from '@testing-library/react'
import TodoComponent from './TodoComponent.jsx';
import { renderWithRouterMatch } from '../../setupTests.js'
import userEvent from '@testing-library/user-event'
import mockedAxios from 'axios'

jest.mock('axios');

const input_values = async ({ description, targetDate}) => {
    await screen.findByTestId('descriptionInput');
    await screen.findByTestId('targetDateInput');
    
    await userEvent.type(screen.getByTestId('descriptionInput'), description)
    await userEvent.type(screen.getByTestId('targetDateInput'), targetDate)

    await userEvent.click(screen.getByTestId('submit'))
}

function arrange({id}){
        //Arrange
        const dataGet = {data: [
            {id: id, username: "andy", description: "Learn to Dance", targetDate: 1607863270787, done: false}
        ]};

        mockedAxios.get.mockImplementationOnce(() => Promise.resolve(dataGet));  

        const tree = renderWithRouterMatch(TodoComponent, {
            route: `/todo/${id}`,
            path: "/todo/:id"
          });  

          return tree
}

describe('TodoComponent', function(){
    //https://scottsauber.com/2019/05/25/testing-formik-with-react-testing-library/
    //https://www.google.com/search?q=react+testing+library+formik+testing&oq=react+testing+library+formik+testing&aqs=chrome..69i57j0i22i30i457j69i60j69i61.4164j0j4&sourceid=chrome&ie=UTF-8
    //Formik validation happens asynchronously,
    //So the solution then is to make the call to find the UI asynchronously via findByTestId instead of getByTestId
    //All of the findBy* functions in react-testing-library are asynchronous and react-testing-library will wait up 
    //to 4.5 seconds for the UI to appear before failing the test, which should give Formik enough time to run validation in this case.
    test('TodoComponent Renders for Update', async () => {
        //Arrange
        const tree = arrange({id : 1}) 

        //Assert
        expect(tree).toMatchSnapshot();
    })

    test('TodoComponent Renders for Add', async () => {
        //Arrange
        const tree = arrange({id : -1}) 

        //Assert
        expect(tree).toMatchSnapshot();
    })    

    test.skip('TodoComponent Submit Update No Errors', async () => {
        //Test rendering error for an createEvent == null
        
        //Arrange
        arrange({id : 0}) 
        
        //Act
        input_values({ description: 'Learn to Test', targetDate: "2020-12-14" })
        
        //Assert
        expect(await screen.queryByTestId('descriptionError')).toBeNull() // it doesn't exist
        expect(await screen.queryByTestId('targetDateError')).toBeNull() // it doesn't exist      
    })

    test('TodoComponent Submit Update Description Error - too few characters', async () => {
        //Arrange
        arrange({id : 0}) 
        
        //Act
            input_values({ description: 'Test', targetDate: "2020-12-14" })        
        
        //Assert
            await screen.findByTestId('descriptionError');
            expect(screen.queryByTestId('descriptionError')).toHaveTextContent(/enter at least 5 characters for description/i)
            console.log('passed')
            
    })

    test('TodoComponent Submit Description Error - blank submission', async () => {
        //Arrange
        arrange({id : 0}) 
         
        //Act
            input_values({ description: undefined, targetDate: "2020-12-14" })        
        
        //Assert
            await screen.findByTestId('descriptionError');
            expect(screen.queryByTestId('descriptionError')).toHaveTextContent(/enter a description/i)
            expect(screen.queryByTestId('targetDateError')  ).toBeNull() // it doesn't exist  
    })
})
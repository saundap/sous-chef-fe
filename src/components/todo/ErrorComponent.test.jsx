import React from 'react';
import { screen } from '@testing-library/react';
import ErrorComponent from './ErrorComponent';
import { renderWithRouterMatch } from '../../setupTests.js'


describe('ErrorComponent', function(){
    test('Error renders', () => {
      //Arrange
      const tree = renderWithRouterMatch(ErrorComponent, {
        route: "/some/bad/route"
        }); 
      //Assert
      expect(tree).toMatchSnapshot();
    })
    
    test('landing on a bad page', () => {
        //Arrange
        renderWithRouterMatch(ErrorComponent, {
        route: "/some/bad/route"
        });
      
        //Assert
        expect(screen.getByTestId('errorcomponent')).toBeInTheDocument()
      })
})
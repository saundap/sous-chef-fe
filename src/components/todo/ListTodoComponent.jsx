import React, {Component} from 'react'
import { deleteTodo, retrieveAllTodos } from '../../api/todo/TodoDataService.js'
import AuthenticationService from '../todo/AuthenticationService.js'
import moment from "moment";
import '../todo/css/Todo.css'

class ListTodoComponent extends Component {
    constructor(props) {
        super(props)
        //console.log('constructor')
        this._isMounted = false;
        this.state = {
            todos :  [],
            message: null
        }

        this.deleteTodoClicked = this.deleteTodoClicked.bind(this)
        this.updateTodoClicked = this.updateTodoClicked.bind(this)
        this.addTodoClicked = this.addTodoClicked.bind(this)
        this.handleSuccessResponse = this.handleSuccessResponse.bind(this)
        this.refreshTodos = this.refreshTodos.bind(this)
    }

    componentWillUnmount(){
        //console.log('componentWillUnmount')
        this._isMounted = false;
    }

    shouldComponentUpdate(nextProps, nextState){
        //console.log('shouldComponentUpdate')
        //console.log(nextProps)
        //console.log(nextState)
        return true
    }

    componentDidMount() {
        console.log('mount')
        this._isMounted = true;
        this.refreshTodos()
    }
    

    deleteTodoClicked(id){
        console.log('deleteTodoClicked ' +id)
        let username = AuthenticationService.getLoggedInUserName()
        console.log(id + " " + username)
        deleteTodo(username, id)
        .then(response => this.handleSuccessResponse(response, id))
        //not implemented .catch(error => this.handleErrorResponse(error))
    }

    addTodoClicked(){
        console.log('updateTodoClicked')
        this.props.history.push(`/todo/-1`)       
    }

    updateTodoClicked(id){
        console.log('updateTodoClicked ' +id)
        this.props.history.push(`/todo/${id}`)
    }    

    handleSuccessResponse(response, id){
        console.log('handleSuccessResponse')
        //console.log(response)
        this.setState({ 
            message: `Delete of todo ${id} successful` 
        })
        this.refreshTodos()
    }

    
    refreshTodos() {
        console.log('refreshTodos')
        let username = AuthenticationService.getLoggedInUserName()
        retrieveAllTodos(username)
            .then(
                response => {
                    console.log(response)
                    this.setState ({
                        todos: response.data
                    })
                }
            )
    }

    render() {
        //console.log('render')
            return (
                <div className="container">
                    <div className="ListTodoComponent">
                        <h1 data-testid="listtodoheader">List Todo</h1>
                        <div>
                            {this.state.message}
                        </div>
                        {this.state.message && <div className="alert alert-success" data-testid="deletedmessage">{this.state.message}</div>}
                        <table className="table" data-testid="todotable">
                            <thead>
                                <tr>
                                    <th>Description</th>
                                    <th>Is Completed</th>
                                    <th>Target Date</th>
                                    <th>Update</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.todos.map (
                                        todo => 
                                            <tr key={todo.id}>
                                                <td>{todo.description}</td>
                                                <td>{todo.done.toString()}</td>
                                                <td>{moment(todo.targetDate).format('MMM DD, YYYY')}</td>
                                                <td><button data-testid={`update-${todo.id}`} className="btn btn-success" onClick={() => this.updateTodoClicked(todo.id)}>Update</button></td>
                                                <td><button data-testid={`delete-${todo.id}`} className="btn btn-warning" onClick={() => this.deleteTodoClicked(todo.id)}>Delete</button></td>
                                            </tr> 
                                        )
                                }
                            </tbody>
                        </table>
                        <button className="btn btn-primary" data-testid="addtodo" onClick={this.addTodoClicked}>Add</button>
                    </div>
                </div>
            )
    }
}

export default ListTodoComponent
import React, {Component} from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import HeaderComponent from './HeaderComponent'
import FooterComponent from './FooterComponent'
import AuthenticatedRoute from './AuthenticatedRoute'
import LoginComponent from './LoginComponent'
import LogOutComponent from './LogOutComponent'
import WelcomeComponent from './WelcomeComponent'
import ListTodoComponent from './ListTodoComponent'
import ListDishesComponent from '../dishes/ListDishesComponent'
import DishComponent from '../dishes/DishComponent'
import DishAddComponent from '../dishes/DishAddComponent'
import TodoComponent from './TodoComponent'
import ErrorComponent from './ErrorComponent'
import PropTypes from 'prop-types'


var loginPage = "/login"
var todoPage = "/todos"
var logoutPage = "/logout"
var dishesPage = "/dishes"


class TodoApp extends Component {
    render() {
        return (
            <div className="TodoApp">
                {/*Routing via react-router-dom npm library */}
                <Router>
                    <>
                        <HeaderComponent/>
                            {/**switch prevents more than one route path from matching */}
                            <Switch>
                                {/*exact attribute limits react-router matching our default for calls to other paths
                                the slash is common on all other paths and react-route will choose it first
                                everytime without exact*/}
                                <Route path="/" exact component={LoginComponent}/>
                                <Route path={loginPage} component={LoginComponent}/>
                                <Route path={logoutPage} component={LogOutComponent}/>
                                {/** Parameter defined in Route:
                                 * The 'name' parameter represents the usage and value passed via the call
                                 * made later in the script using this.props.history.push("/welcome/${this.state.username}
                                 * This is then passed to the welcome component as a params prop, this.state.match.params.name")
                                 */}
                                <AuthenticatedRoute path="/welcome/:name" component={WelcomeComponent}/>
                                <AuthenticatedRoute path={todoPage} component={ListTodoComponent}/>
                                <AuthenticatedRoute path="/todo/:id" component={TodoComponent}/>
                                <AuthenticatedRoute path="/dishes/-1" component={DishAddComponent}/>
                                <AuthenticatedRoute path="/dishes/:id" component={DishComponent}/>
                                <AuthenticatedRoute path={dishesPage} component={ListDishesComponent}/>
                                {/*route error handling*/}
                                <AuthenticatedRoute path="" component={ErrorComponent}/>
                            </Switch>
                        {/*<FooterComponent/>*/}
                    </>
                </Router>
            </div>
        )
    }
}


export default TodoApp
import React from 'react'

function ErrorComponent() {
    return <div className="ErrorComponent" data-testid="errorcomponent">An Error Occurred: Contact support at saundap@gmail.com</div>
}

export default ErrorComponent
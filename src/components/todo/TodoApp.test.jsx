import React from 'react';
import { render, screen } from '@testing-library/react';
import { screen as domScreen} from '@testing-library/dom'
import TodoApp from './TodoApp';
import { renderWithRouterMatch } from '../../setupTests.js'

describe('TodoApp', function(){

    test('Todo page renders as expected at load', () => {   
        const tree = render(<TodoApp />);  
        expect(tree).toMatchSnapshot();
    })

    test('Route Guessing Not AuthenticatedRoute - Try to Visit /todo - Redirect back to Login', () => {
        //Arrange
        sessionStorage.setItem('authenticatedUser', null);

        const { getByTestId } = renderWithRouterMatch(TodoApp, {
            route: "/todos"
            });
        
        //Act
        domScreen.debug()

        //Assert
        expect(getByTestId('loginheader')).toHaveTextContent("Login Page")
        expect(screen.queryByTestId('todo_link')).not.toBeInTheDocument()
    })

    test('Route Guessing Is AuthenticatedRoute - Header Links are present', () => {
        sessionStorage.setItem('authenticatedUser', 'andy');

        //Arrange
         const { getByTestId } = renderWithRouterMatch(TodoApp, {
            route: "/todos"
            });

        //Act
        //Go to ToDo Page
        domScreen.debug()

        //Assert
        expect(getByTestId('loginheader')).toHaveTextContent("Login Page")
        expect(screen.queryByTestId('todo_link')).toBeInTheDocument()
    })



})
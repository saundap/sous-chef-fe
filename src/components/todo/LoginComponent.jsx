import React, {Component} from 'react'
import AuthenticationService from './AuthenticationService'
import PropTypes from 'prop-types'

class LoginComponent extends Component{
    constructor(props) {
        super(props)
        this.state = {
            username: 'andy',
            password: '',
            hasLoginFailed: false,
            showSuccessMessage: false
        }

    this.handleChange = this.handleChange.bind(this)
    this.loginClicked = this.loginClicked.bind(this)
    }
   
    render() {
        return (
            <>
                <div className="LoginComponent">
                    <h1 data-testid="loginheader">Login Page</h1>
                    <div className="container">
                        {/*conditional rendering
                        if first condition is true then the second will resolve*/}
                        {this.state.hasLoginFailed && <div className="alert alert-warning">Invalid Credentials</div>} 
                        {this.state.showSuccessMessage && <div>Successful Login</div>}
                        User Name: <input type="text" name="username" value={this.state.username} onChange={this.handleChange} data-testid="username"/>
                        Password: <input type="text" name="password" value={this.state.password} onChange={this.handleChange} data-testid="password" />
                        <button className="btn-success" onClick={this.loginClicked} data-testid="login">Login</button> 
                    </div>
                </div>
            </>
        )
    }


    //a function inside a class is referred to as a method in js es6
    handleChange(event) {
        //passing in an event via onChange controlled field and this makes the component a controlled component
        //source of truth is maintained in the state
        console.log("handleChange")
        console.log(this.state)
        console.log(`changing ${event.target.name}`)
        //updates Form element fields (target.name) to the matching state property only b/c the property names are equal
        this.setState(
            {
                [event.target.name]: event.target.value
            }
        )
        console.log(this.state)

    }

    loginClicked() {
        console.log("loginClicked")
        
        //HardCoded
        /*if( (this.state.username==='andy' || this.state.username==='trent')  && this.state.password ==='password') {
            //Authentication Session
            AuthenticationService.registerSuccessfulLogin(this.state.username, this.state.password);
            //https://reactrouter.com/web/api/history
            //https://github.com/ReactTraining/history
            //history.push is a re-direct used for routing
            this.props.history.push(`/welcome/${this.state.username}`)
            console.log('successful')
        } else {  
            this.setState({
                hasLoginFailed: true,
                showSuccessMessage: false
            })
            console.log('failed')    
        }*/

        //BasicAuth Flow
        /*AuthenticationService.executeBasicAuthService(this.state.username, this.state.password)
        .then(() => {
            //Authentication Session
            AuthenticationService.registerSuccessfulLogin(this.state.username, this.state.password);
            //https://reactrouter.com/web/api/history
            //https://github.com/ReactTraining/history
            //history.push is a re-direct used for routing
            this.props.history.push(`/welcome/${this.state.username}`)
            console.log('successful')
            }
        ).catch( () => {
            this.setState({
                hasLoginFailed: true,
                showSuccessMessage: false
            })
            console.log('failed')   
        })*/

        //Jwt Flow
        AuthenticationService.executeJwtAuthService(this.state.username, this.state.password)
        .then((response) => {
            //Authentication Session
            AuthenticationService.registerSuccessfulforJwt(this.state.username, response.data.token);
            //https://reactrouter.com/web/api/history
            //https://github.com/ReactTraining/history
            //history.push is a re-direct used for routing
            this.props.history.push(`/welcome/${this.state.username}`)
            console.log('successful')
            }
        ).catch( () => {
            this.setState({
                hasLoginFailed: true,
                showSuccessMessage: false
            })
            console.log('failed')   
        }) 
    }    
}

//Default property value if none is passed
//LoginComponent.defaultProps = {
//    name : 'andy',
//    password: 'password'
//}

//Type check on the passed properties, if not of type defined here then an error thrown to console logs
LoginComponent.propTypes = {
    name : PropTypes.string,
    password: PropTypes.string
}

export default LoginComponent
import React, {Component} from 'react'

class LogOutComponent extends Component {
    render() {
        return (
            <>
                <h1>You are Logged Out</h1>
                {/**the container className is a bootstrap class */}
                <div className="container" data-testid="logout">Thank you for using our application</div> 
            </>
        )
    }
}

export default LogOutComponent
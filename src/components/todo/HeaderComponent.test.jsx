import React from 'react';
import HeaderComponent from './HeaderComponent.jsx';
import { renderWithRouterMatch } from '../../setupTests.js'

describe('HeaderComponent', function(){
    test('Header renders', () => {
        //Arrange
        sessionStorage.setItem('authenticatedUser', 'andy');
        const tree = renderWithRouterMatch(HeaderComponent, {}); 
        //Assert
        expect(tree).toMatchSnapshot();
    }) 
})
import axios from "axios";
import { API_URL } from '../../Constants.js'
export const USER_NAME_SESSION_ATTRIBUTE_NAME='authenticatedUser'

class AuthenticationService {

    executeBasicAuthService(username, password){
        return axios.get(`${API_URL}/basicauth`, {
                        headers: { authorization: this.createBasicAuthToken(username, password)}
                    }
        )
    }

    executeJwtAuthService(username, password){
        return axios.post(`${API_URL}/authenticate`, {
            "username": username,
            "password": password
        })
    }

    createBasicAuthToken(username, password){
        return 'Basic '+ window.btoa(username + ":" + password) //btoa base64 encoding for js
    }

    applyJwtToken(token){
        return 'Bearer '+ token
    }
    

    registerSuccessfulLogin(username,password){
        console.log("registerSuccessfulLogin");
        console.log("Setting Local Store authenticatedUser");
        sessionStorage.setItem(USER_NAME_SESSION_ATTRIBUTE_NAME, username);
        console.log("Local Store authenticatedUser set to "+username);
        //At log in time when session is set then the interceptor is activated
        //pass the username password to the interceptor
        this.setUpAxiosInterceptors(this.createBasicAuthToken(username, password))
    }

    registerSuccessfulforJwt(username,token){
        console.log("registerSuccessfulforJwt");
        console.log("Setting Local Store authenticatedUser");
        sessionStorage.setItem(USER_NAME_SESSION_ATTRIBUTE_NAME, username);
        console.log("Local Store authenticatedUser set to "+username);
        //At log in time when session is set then the interceptor is activated
        //pass the token to the interceptor
        this.setUpAxiosInterceptors(this.applyJwtToken(token))
    }

    logout(){
        sessionStorage.removeItem(USER_NAME_SESSION_ATTRIBUTE_NAME);
    }

    isUserLoggedIn(){
        let user = this.getSessionProperty(USER_NAME_SESSION_ATTRIBUTE_NAME);
        if(user===null || user==='null') return false
        return true
    }

    getLoggedInUserName(){
        let user = this.getSessionProperty(USER_NAME_SESSION_ATTRIBUTE_NAME);
        if(user===null || user==='null') return ''
        return user
    }
    
    getSessionProperty(property){
        return sessionStorage.getItem(property)
    }

    setUpAxiosInterceptors(token) {
        axios.interceptors.request.use(
            //what ever the existing config of the request is the argument here
            (config) => {
                if(this.isUserLoggedIn()){
                    //update and add a header to the request config
                    //this will now be universally applied to any request sent from axios
                    config.headers.Authorization = token
                }
                //return the appended config
                return config
            }
        )
    }

}

export default new AuthenticationService()
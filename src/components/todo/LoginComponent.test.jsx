import React from 'react'
import {screen, fireEvent } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import LoginComponent from './LoginComponent.jsx';
import { renderWithRouterMatch } from '../../setupTests.js'
import { API, USER_NAME_SESSION_ATTRIBUTE_NAME } from '../../components/todo/AuthenticationService.js'
import { API_URL } from '../../Constants.js'
import mockedAxios from 'axios'

jest.mock('axios');

const loginFormTypeValues = ({ username, password}) => {
    console.log('changing username to '+username)
    fireEvent.change(screen.getByTestId('username'), { target: { value: username } });

    console.log('changing password to '+password)
    fireEvent.change(screen.getByTestId('password'), { target: { value: password } });

    console.log('clicking login')
    userEvent.click(screen.getByTestId('login'))
};


describe('LoginComponent', function(){
    test('Conditional LogIn Successful', async () => {
        //Arrange
        const dataGet = {data: {"token":"aojfd;jfadsifjeao3oi"}};
        mockedAxios.post.mockImplementationOnce(() => Promise.resolve(dataGet)); //mock the jwt authenticate request       

        const ui = renderWithRouterMatch(LoginComponent, {
            route: "/login"
          });
        
        //Act & //Assert
        loginFormTypeValues({ username: 'andy', password:'password' });
        expect(mockedAxios.post).toHaveBeenCalledWith(`${API_URL}/authenticate`, {"password": "password", "username": "andy"})
    })

    test.skip('Conditional LogIn Unsuccessful', async () => {
        //Arrange
        const dataGet = {data: {"token":"aojfd;jfadsifjeao3oi"}};
        mockedAxios.post.mockImplementationOnce(() => Promise.resolve(dataGet)); //mock the jwt authenticate request           
       
        const ui = renderWithRouterMatch(LoginComponent, {
            route: "/login"
          });

        //Act & //Assert
        loginFormTypeValues({ username: 'andy', password:'invalid-password' });
        
        //Assert
        const element = screen.getByText(/invalid credentials/i)

        expect(element).toHaveTextContent('Invalid Credentials')
        expect(mockedAxios.post).toHaveBeenCalledWith(`${API_URL}/authenticate`, {"password": "invalid-password", "username": "andy"})
        expect(mockedAxios.post).toHaveBeenCalledTimes(1)
    })
   
    test('Session Storage LogIn', () => {
      //npm install mock-local-storage --save-dev
      //mocks local and session storage across all test once jest.confg.js is configured for "setupFiles" property
      
      //Arrange
      const dataGet = {data: {"token":"aojfd;jfadsifjeao3oi"}};
      mockedAxios.post.mockImplementationOnce(() => Promise.resolve(dataGet)); //mock the jwt authenticate request         
     
      const ui = renderWithRouterMatch(LoginComponent, {
        route: "/login"
      });

      //Act
      loginFormTypeValues({ username: 'andy', password:'password' });
      expect(mockedAxios.post).toHaveBeenCalledWith(`${API_URL}/authenticate`, {"password": "password", "username": "andy"})
      //Assert
      expect(sessionStorage.getItem(USER_NAME_SESSION_ATTRIBUTE_NAME)).toBe('andy');    
    })

})
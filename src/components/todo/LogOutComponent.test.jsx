import React from 'react'
import '@testing-library/jest-dom/extend-expect'
import { screen, findByText } from '@testing-library/react'
import LogOutComponent from './LogOutComponent.jsx';
import { renderWithRouterMatch } from '../../setupTests.js'

describe('LogOutComponent', function(){
    test('LogOut Renders', async () => {
        //Arrange
        renderWithRouterMatch(LogOutComponent, {
            route: "/logout"
          });
        
        //await waitFor(() => screen.getByText(/You are Logged Out/i));
        await screen.findByText(/You are Logged Out/i);

        //Assert
        const element = screen.getByTestId("logout")
        expect(element).toHaveTextContent('Thank you for using our application')

    })

})
import React from 'react'
import '@testing-library/jest-dom/extend-expect'
import {findByText, screen, waitFor} from '@testing-library/react'
import WelcomeComponent from './WelcomeComponent.jsx';
import { renderWithRouterMatch } from '../../setupTests.js'
import userEvent from '@testing-library/user-event'
import mockedAxios from 'axios'
import { URL } from '../../api/todo/HelloWorldService.js'

jest.mock('axios');

describe('WelcomeComponent', function(){
    
    test('WelcomeComponent Renders', async () => {
        //Arrange
        renderWithRouterMatch(WelcomeComponent, {
            route: "/welcome/ABC123",
            path: "/welcome/:name"
          });

        //Act & Assert

        //await waitFor(() => screen.getByText(/Welcome ABC123/i));
        await screen.findByText(/Welcome ABC123/i);


        const element = screen.getByTestId("welcome")
        expect(element).toHaveTextContent('ABC123')
    })

    test('Hello World Service button success', async () => {
         /**cannot get the message prop for welcome to display
         * the button is clicked and the mock service works and the response error is received
         * may be related to failure in test to render error message
         */
        
        //Arrange
        const data = {data: {message: 'Hello World Bean ABC123'}, status: 200, statusText: '', headers: {}, config: {}, request: {}}
        mockedAxios.get.mockImplementationOnce(() => Promise.resolve(data));

        renderWithRouterMatch(WelcomeComponent, {
            route: "/welcome/ABC123",
            path: "/welcome/:name"
            });

        //Act
        userEvent.click(screen.getByTestId('helloworldbutton'))

        //Assert
        await screen.findByText(/hello world bean ABC123/i);

        ///screen.debug()
        expect(mockedAxios.get).toHaveBeenCalledWith(URL+'/ABC123')
        expect(mockedAxios.get).toHaveBeenCalledTimes(1)
    })

    test('Hello World Service button error', async () => {
         /**cannot get the conditional render to work in test for error
         * the button is clicked and the mock service works and the response error is received
         */

        //Arrange
        const data = {  "timestamp": 1608400234019, 
                        "status": 500, 
                        "error": "Internal Server Error", 
                        "trace": "stack trace", 
                        "message": "Something went wrong with HelloWorldBean service",
                        "path": "/helloworldbeanerror/andy" 
                    }
        mockedAxios.get.mockImplementationOnce(() => Promise.reject(data));

        renderWithRouterMatch(WelcomeComponent, {
            route: "/welcome/ABC123",
            path: "/welcome/:name"
            });

        //Act
        userEvent.click(screen.getByTestId('helloworldbutton'))

        //Assert
        console.log('debuggin screen')
        screen.debug()
        await screen.findByText(/Something went wrong with HelloWorldBean service/i);

        expect(mockedAxios.get).toHaveBeenCalledWith(URL+"/ABC123")
        expect(mockedAxios.get).toHaveBeenCalledTimes(1)    })
})
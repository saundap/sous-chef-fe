import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

describe('App', function(){

  test.skip('renders learn react link', () => {
    render(<App />);
    const linkElement = screen.getByText(/My Todo Application/i);
    expect(linkElement).toBeInTheDocument();
  })
})

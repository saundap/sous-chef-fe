import React, { Component } from 'react';
//import FirstComponent, {SecondComponent} from './components/learning-examples/ClassComponents'
//import ThirdComponent from './components/learning-examples/FunctionComponents'
//import Counter from './components/counter/Counter'
import TodoApp from './components/todo/TodoApp'
import InviteFriends from './components/formik-field-array/InviteFriends'
import './App.css';
import './bootstrap.css';


//Future changes to react are for App.js to be a function component and NOT a class component
class App extends Component {

  render() {
    return (
      <div className="App">
        {/*<LearningComponents></LearningComponents>*/}
        {/*<Counter/>*/}
        {/*<TodoApp/>*/}
        {/*<InviteFriends/>*/}
        <TodoApp/>
      </div>
    );
  }
}

/*{class LearningComponents extends Component {
  render() {
    return (
      <div className="LearningComponents">
        <header className="App-header">
            My Sous-Chef App
        </header>
        <FirstComponent></FirstComponent>
        <SecondComponent></SecondComponent>
        <ThirdComponent></ThirdComponent>
      </div>
    );
  }
}*/

export default App;    
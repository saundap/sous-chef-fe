context('Working with a Todo', () => {
    beforeEach(() => {
      
    //Arrange
    // not useful since i log in before eveyr test cy.window().then(win => win.sessionStorage.setItem('authenticatedUser','andy'));    
        cy.visit("/")
        cy.login('andy', 'password')
        cy.wait(2000)
    })

    it('todo page loads for a user', () => {
        cy.get('[data-testid="todo_link"]').click()
        cy.get('[data-testid="update-10002"]').click()
        
        //Act & Assert      
        cy.get('[data-testid="descriptionInput"]').then($element => {
            if ($element.is(':visible')){
              //you get here only if button is visible
            }
          })

        cy.get('[data-testid="targetDateInput"]').then($element => {
            if ($element.is(':visible')){
                //you get here only if button is visible
            }
        })

        cy.get('[data-testid="todoheader"]').then($element => {
            if ($element.is(':visible')){
                //you get here only if button is visible
                cy.get('[data-testid="todoheader"]').contains(/Todo/i)
            }
        })              
    })

    it('todo update for a user', () => {
        
        cy.get('[data-testid="todo_link"]').click()
        cy.get('[data-testid="update-10002"]').click()

        //Arrange
            const todaysDate = Cypress.moment(7977788111).format('YYYY-MM-DD')

        //Act & Assert      
        cy.get('[data-testid="descriptionInput"]').then($element => {
            if ($element.is(':visible')){
              //you get here only if button is visible
              cy.get('[data-testid="descriptionInput"]').clear()

              cy.get('[data-testid="descriptionInput"]').type('test description')
            }
          })

        cy.get('[data-testid="targetDateInput"]').then($element => {
            if ($element.is(':visible')){
                //you get here only if button is visible
                cy.get('[data-testid="targetDateInput"]').clear()
                cy.get('[data-testid="targetDateInput"]').type(todaysDate)
            }
        })

        cy.get('[data-testid="submit"]').then($button => {
            if ($button.is(':visible')){
                //you get here only if button is visible
                cy.get('[data-testid="submit"]').click()
            }
        })
        
        //Re-direct back to todos listing after update
        cy.get('[data-testid="listtodoheader"]')
            .contains(/list todo/i)
            
            cy.get('[data-testid="todotable"]')
            .contains(/test description/i)    
            cy.get('[data-testid="todotable"]')
            .contains(Cypress.moment(todaysDate).format('MMM DD, YYYY'))
    })

    it('todo update description error for a user', () => {
        //Arrange
            cy.get('[data-testid="todo_link"]').click()
            cy.get('[data-testid="update-10003"]').click()

            const todaysDate = Cypress.moment().format('YYYY-MM-DD')

        //Act & Assert      
        cy.get('[data-testid="descriptionInput"]').then($element => {
            if ($element.is(':visible')){
              //you get here only if button is visible
              cy.get('[data-testid="descriptionInput"]').clear()
              cy.get('[data-testid="descriptionInput"]').type('test')
            }
          })

        cy.get('[data-testid="targetDateInput"]').then($element => {
            if ($element.is(':visible')){
                //you get here only if button is visible
                cy.get('[data-testid="targetDateInput"]').clear()
                cy.get('[data-testid="targetDateInput"]').type(todaysDate)
            }
        })

        cy.get('[data-testid="submit"]').then($button => {
            if ($button.is(':visible')){
                //you get here only if button is visible
                cy.get('[data-testid="submit"]').click()
            }
        })
        
        cy.get('[data-testid="descriptionerror"]').then($button => {
            if ($button.is(':visible')){
                //you get here only if button is visible
                cy.get('[data-testid="descriptionerror"]').contains(/Enter at least 5 characters for Description/i)
            }
        })       
        
    })  

    it('Add a todo', () => {
        //Arrange
        cy.get('[data-testid="welcome"]')
        cy.get('[data-testid="todo_link"]').click()

        const todaysDate = Cypress.moment(1603368911).format('YYYY-MM-DD')
        cy.server().route('POST', '/jpa/mongo/users/andy/todo').as('postURI');
        cy.server().route('GET', '/jpa/mongo/users/andy/todos').as('getURI');

    //Act    
        cy.get('[data-testid="addtodo"]').click({force: true})
        cy.get('[data-testid="descriptionInput"]').then($element => {
            if ($element.is(':visible')){
            //you get here only if button is visible
            cy.get('[data-testid="descriptionInput"]').clear()

            cy.get('[data-testid="descriptionInput"]').type('A new todo')
            }
        })

        cy.get('[data-testid="targetDateInput"]').then($element => {
            if ($element.is(':visible')){
                //you get here only if button is visible
                cy.get('[data-testid="targetDateInput"]').clear()
                cy.get('[data-testid="targetDateInput"]').type(todaysDate)
            }
        })

        cy.get('[data-testid="submit"]').then($button => {
            if ($button.is(':visible')){
                //you get here only if button is visible
                cy.get('[data-testid="submit"]').click({force: true})
            }
        })

        //Assert      
        // Wait for response.status to be 201
            cy.wait(['@postURI', '@getURI']); 
            
        //Re-direct back to todos listing after update
            cy.get('[data-testid="listtodoheader"]')
            .contains(/list todo/i)
            
            cy.get('[data-testid="todotable"]')
            .contains(/a new todo/i)    
            cy.get('[data-testid="todotable"]')
            .contains(Cypress.moment(todaysDate).format('MMM DD, YYYY'))
    })

})
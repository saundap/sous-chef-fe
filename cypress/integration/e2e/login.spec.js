context('LogIn', () => {
    beforeEach(() => {
      cy.visit("/")
    })
  
    // https://on.cypress.io/interacting-with-elements
  
    it('login succesfully', () => {
      // https://on.cypress.io/type
        
      //Arrange
        cy.login('andy', 'password')
      //Assert      
        cy.get('[data-testid="welcome"]')
            .contains(/welcome andy. you can manage your todos/i)
    })

    it('login unsuccesfully', () => {
        // https://on.cypress.io/type
          
        //Arrange
          cy.login('andy', 'invalidtest')
        //Assert      
          cy.get('[class="alert alert-warning"]')
              .contains(/invalid credentials/i)
    })

})
  
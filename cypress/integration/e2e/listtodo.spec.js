context('Working with the List Todos', () => {
    beforeEach(() => {
      
    //Arrange
    // not useful since i log in before eveyr test cy.window().then(win => win.sessionStorage.setItem('authenticatedUser','andy'));    
    cy.visit("/")
    cy.login('andy', 'password')
    })

    it('todo list loads for a user', () => {
        //Act
        cy.get('[data-testid="todo_link"]').click()

        //Assert
        cy.get('[data-testid="todotable"]').then($button => {
            if ($button.is(':visible')){
              //you get here only if button is visible
            }
          })
    })

    it('delete a todo', () => {
        //Act
        cy.get('[data-testid="todo_link"]').click()
        cy.get('[data-testid="delete-10001"]')
            .click()
        //Assert      
        cy.get('[data-testid="deletedmessage"]')
            .contains(/delete of todo 1 successful/i)
    })

    it('add a todo', () => {
        //Act
        cy.get('[data-testid="todo_link"]').click()
        cy.get('[data-testid="addtodo"]')
            .click()
        //Assert      
        cy.get('[data-testid="todoheader"]')
            .contains(/todo/i)

        cy.get('[data-testid="submit"]').should('be.visible') 
    })    

})
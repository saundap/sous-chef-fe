context('Header Routing', () => {
    beforeEach(() => {
      cy.visit("/")
    })

    
    it('Session Store is Created', () => {
        //Arrange & Act
            cy.login('andy', 'password')
        //Assert      
        cy.window()
        .its("sessionStorage")
        .invoke("getItem", "authenticatedUser")
        .should("exist")
        .should('eq', 'andy')
     })

     it('Session Store is Removed', () => {
        //Arrange & Act
        cy.login('andy', 'password')
        
        cy.window()
        .its("sessionStorage")
        .invoke("getItem", "authenticatedUser")
        .should("exist")
        .should('eq', 'andy')

        cy.get('[data-testid="logout_link"]').click()
        //Assert      
        cy.window()
        .its("sessionStorage")
        .invoke("getItem", "authenticatedUser")
        .should("not.exist")
     })

     it('Session Store is Not Created with Failed Login', () => {
      //Arrange & Act
      cy.login('andy', 'test')
      //Assert      
      cy.window()
      .its("sessionStorage")
      .invoke("getItem", "authenticatedUser")
      .should("not.exist")
   })

})
context('Welcome Page Message API Handling', () => {
    beforeEach(() => {
      
    //Arrange
        cy.visit("/")
        cy.login('andy', 'password')     
        cy.window()
        .its("sessionStorage")
        .invoke("getItem", "authenticatedUser")
        .should("exist")
        .should('eq', 'andy')

    })

    it('welcome with resolve message', () => {
        //Act
        cy.get('[data-testid="helloworldbutton"]')
            .click()
        //Assert      
        cy.get('[data-testid="welcomemessage"]')
            .contains(/Hello World Bean/i)
    })

})
context('Header Routing', () => {
    beforeEach(() => {
      cy.visit("/")
    //Arrange
      cy.login('andy', 'password')
    })

    function checkHeaderLinks() {
      cy.get('data-testid="home_link"').should('exist')
      cy.get('data-testid="todo_link"').should('exist')
      cy.get('data-testid="logout_link"').should('exist')
      cy.get('[data-testid="branding"]').should('exist')
    }
    
    it('Click Home Link', () => {
      //Act
        cy.wait(3000) //wait as is quicker than session authuser value being set - couls try on checking the session authuser
        cy.get('[data-testid="home_link"]').click()
      //Assert      
        cy.get('[data-testid="welcome"]')
            .contains(/you can manage your todos/i)
        checkHeaderLinks
      //Act
        cy.get('[data-testid="logout_link"]').click()
    })

    it('Click Todo Link', () => {
      //Act
        cy.get('[data-testid="todo_link"]').click()
      //Assert      
        cy.get('[data-testid="listtodoheader"]')
            .contains(/list todo/i)
        cy.get('[class="table"]').should('exist')
        checkHeaderLinks
      //Act
        cy.get('[data-testid="logout_link"]').click()        
    })
    
    it('Click Logout Link', () => {
      //Act
        cy.get('[data-testid="logout_link"]').click()
      //Assert      
        cy.get('[data-testid="todo_link"]').should('not.exist')
        cy.get('[data-testid="home_link"]').should('not.exist')
        
        cy.get('[data-testid="logout"]')
            .contains(/Thank you for using our application/i)
        
        cy.get('[data-testid="branding"]').should('exist')        
    })

})
  
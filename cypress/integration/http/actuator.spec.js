
context('Actuator Up Test', () => {

    //https://docs.cypress.io/api/commands/request.html#Arguments
    
    let backendUrl = 'http://localhost:8080'
    
        it('Check that the actuator service returns up when spring boot be is running', () => {
            cy.request( {
                method: 'GET',
                url: `${backendUrl}/actuator/health`,
                failOnStatusCode: false
            }).then((response) => {
                // response.body is automatically serialized into JSON
                expect(response.status).to.eq(200)
                expect(response.body.status).to.eq("UP")
            })
        })
})
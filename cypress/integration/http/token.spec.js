context('JWT Token Tests', () => {

//https://docs.cypress.io/api/commands/request.html#Arguments

let backendUrl = 'http://localhost:8080'

    it('No token supplied - 401', () => {
        cy.request( {
            method: 'GET',
            url: `${backendUrl}/users/andy/todos`,
            failOnStatusCode: false
        }).then((response) => {
            // response.body is automatically serialized into JSON
            expect(response.status).to.eq(401)
        })
    })


    it('Token acquired 200', () => {
        let token = null;
        cy.request({
            method: 'POST',
            url: `${backendUrl}/authenticate`, // baseUrl is prepended to url
            body: {
              username: 'andy',
              password: 'password'
            }
          }).then((response) => {
            token = response.body.token
          })

          cy.request( {
            method: 'GET',
            url: `${backendUrl}/users/andy/todos`,
            headers: {Authorization: 'Bearer '+token},
            failOnStatusCode: false
        }).then((response) => {
            // response.body is automatically serialized into JSON
            expect(response.status).to.eq(401)
        })
    })
    

})
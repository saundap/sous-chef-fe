context('Welcome Page Message API Error Handling', () => {
    beforeEach(() => {
      
    //Arrange
    cy.window().then(win => win.sessionStorage.setItem('authenticatedUser','andy'));    
    cy.visit("/welcome/andy")

    /*
    cy.intercept("GET", "/helloworldbean/andy",  {fixture: "welcome_error.json"}).as('error_response')
    */

    })

    it('welcome with resolve message', () => {
        //Arrange
            cy.server()
            cy.route({
                method: 'GET',
                url: 'http://localhost:8080/helloworldbean/*',
                status: 200,
                response: {
                    message: 'Welcome to the Page!',
                }
            }).as('error_response')

        //Act
        cy.get('[data-testid="helloworldbutton"]')
            .click()
        cy.wait('@error_response')    
        //Assert      
        cy.get('[data-testid="welcomemessage"]')
            .contains(/Welcome to the Page!/i)
    })

    it('welcome with error', () => {
        //Arrange
        cy.server()
        cy.route({
            method: 'GET',
            url: 'http://localhost:8080/helloworldbean/*',
            status: 500,
            response: {
                message: 'Something went wrong, please try again later',
            }
        }).as('error_response')
        
        //Act
        cy.get('[data-testid="helloworldbutton"]')
            .click()
        cy.wait('@error_response')    
        //Assert      
        cy.get('[data-testid="errormessage"]')
            .contains(/Something went wrong, please try again later/i)
    })

})
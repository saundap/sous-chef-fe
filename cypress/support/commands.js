// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
Cypress.Commands.add("login", (username, password) => { 
    //Arrange
        cy.get('[data-testid="username"]')
          .clear()
          .type(username)
        cy.get('[data-testid="password"]')
          .type(password)
    //Act  
         cy.get('[data-testid="login"]')
          .click()
 })

 Cypress.Commands.add("checkIfEleExists", (ele) => {
      /// here if  ele exists or not
      cy.get('body').find( ele ).its('length').then(res=>{
          if(res > 0){
              resolve();
          }else{
              reject();
          }
      });
})

//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

const path = require('path');

// // mocks .svg, see "moduleNameMapper in jest.config.js": {

module.exports = {
  process(src, filename) {
    return `module.exports = ${JSON.stringify(path.basename(filename))}`;
  }
};
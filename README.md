# sous-chef-fe


# username = andy
# password = password
# java/com/birchfield/services/restfulwebservicejwtauth/auth/jwt/JwtInMemoryUserDetailsService.java

//how-to guide for test library using jest framework
https://testing-library.com/docs

//jest-dom testing-library custom matches for assertions
https://github.com/testing-library/jest-dom#custom-matchers

npm run test-jest -- -u //update snapshots
npm run test-jest-single -t 'TodoApp'
npm run test-jest
npx cypress open

 ## NEW INFO: 
    - one of the benefits of using arrow functions, that they take the value of "this" of its parent so we no longer need to make the binding.


# Mocking Session Store
      - npm install mock-local-storage --save-dev
      - mocks local and session storage across all test once jest.confg.js is configured for "setupFiles" property

# Multiple AXIOS Gets:
        mockedAxios.get.mockImplementation((url) => {
            switch (url) {
              case '/users.json':
                return Promise.resolve({data: [{name: 'Bob', items: []}]})
              case '/items.json':
                return Promise.resolve({data: [{id: 1}, {id: 2}]})
              default:
                return Promise.reject(new Error('not found'))
            }
          })